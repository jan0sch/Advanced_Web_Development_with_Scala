/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.api

import cats.effect._
import cats.implicits._
import com.wegtam.books.awdewis.models._
import eu.timepit.refined.auto._
import eu.timepit.refined.cats._
import eu.timepit.refined.types.string.NonEmptyString
import org.http4s._
import org.http4s.circe._
import org.http4s.dsl._
import sttp.model._
import sttp.tapir._
import sttp.tapir.codec.refined._
import sttp.tapir.json.circe._
import sttp.tapir.server.http4s._

final class HelloWorld[F[_]: Sync: ContextShift] extends Http4sDsl[F] {
  final val message: NonEmptyString = "This is a fancy message directly from http4s! :-)"

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit def decodeGreetings: EntityDecoder[F, Greetings] = jsonOf
  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  implicit def encodeGreetings: EntityEncoder[F, Greetings] = jsonEncoderOf

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  private val sayHello: HttpRoutes[F] = HelloWorld.greetings.toRoutes { name =>
    val greetings = (
      NonEmptyString.from(s"Hello ${name.show}!").toOption,
      NonEmptyString.from(s"Hello ${name.show}, live long and prosper!").toOption
    ).mapN {
      case (title, headings) =>
        Greetings(
          title = title,
          headings = headings,
          message = message
        )
    }
    Sync[F].delay(greetings.fold(StatusCode.BadRequest.asLeft[Greetings])(_.asRight[StatusCode]))
  }

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  val routes: HttpRoutes[F] = sayHello

}

object HelloWorld {
  val example = Greetings(
    title = "Hello Kirk!",
    headings = "Hello Kirk, live long and prosper!",
    message = "This is some demo message..."
  )

  @SuppressWarnings(Array("org.wartremover.warts.Any"))
  val greetings: Endpoint[NonEmptyString, StatusCode, Greetings, Nothing] =
    endpoint.get
      .in("hello")
      .in(query[NonEmptyString]("name"))
      .errorOut(statusCode)
      .out(jsonBody[Greetings].description("A JSON object demo").example(example))
      .description(
        "Returns a simple JSON object using the provided query parameter 'name' which must not be empty."
      )
}
