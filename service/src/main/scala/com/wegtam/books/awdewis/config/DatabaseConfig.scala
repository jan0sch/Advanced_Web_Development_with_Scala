/*
 * CC0 1.0 Universal (CC0 1.0) - Public Domain Dedication
 *
 *                                No Copyright
 *
 * The person who associated a work with this deed has dedicated the work to
 * the public domain by waiving all of his or her rights to the work worldwide
 * under copyright law, including all related and neighboring rights, to the
 * extent allowed by law.
 */

package com.wegtam.books.awdewis.config

//import java.nio.charset.StandardCharsets

//import cats.implicits._
import com.wegtam.books.awdewis.db._
import eu.timepit.refined.auto._
import eu.timepit.refined.pureconfig._
import pureconfig._
import pureconfig.generic.semiauto._
//import pureconfig.error.CannotConvert

/**
  * The configuration for a database connection.
  *
  * @param driver The class name of the JDBC driver.
  * @param url    A JDBC URL.
  * @param user   The username for the connection.
  * @param pass   The password for the connection.
  */
final case class DatabaseConfig(
    driver: JDBCDriverName,
    url: JDBCUrl,
    user: JDBCUsername,
    pass: JDBCPassword
)

object DatabaseConfig {
  // The default configuration key to lookup the database configuration.
  final val CONFIG_KEY: ConfigKey = "database"

  // We need a custom reader for our JDBCPassword because of the String to Array[Byte] conversion.
//  implicit val jdbcPasswordReader: ConfigReader[JDBCPassword] =
//    ConfigReader[String].emap(s =>
//      JDBCPassword
//        .from(s.getBytes(StandardCharsets.UTF_8))
//        .leftMap(e => CannotConvert(s, "JDBCPassword", e))
//    )

  implicit val configReader: ConfigReader[DatabaseConfig] = deriveReader[DatabaseConfig]

}
